Project created in my spare time, using ReactJS, RainbowKit and Wagmi.

Project written in Solidity.
Deploy on Mumbai Testnet (Polygon)

Smart Contract available here : https://mumbai.polygonscan.com/address/0x51A58868FD05fd551d6dE7C4a59362ed32E3f626#code
